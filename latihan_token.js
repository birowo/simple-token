function b64safe(str){
  var ret = ''
  for(var i=0; i<str.length; i++){
    var chr = str.charAt(i)
    switch(chr){
    case '/':
      ret += '-'
      break
    case '+':
      ret += '_'
      break
    case '=':
      break
    default:
      ret += chr
    }
  }
  return ret
}

function root(req, res, url){
  res.setHeader('Content-Type', 'text/html')
  res.end('<a href="/token" target="_blank">GET TOKEN</a>')
}
const crypto = require('crypto')
const secret = 's3cr3tC0D3'
const algo = 'sha384'
const count1 = 20, count2 = 10
const latency = 5
const tknLbl = 'tkn=', tknLblLen = tknLbl.length
function token(req, res, url){
  const kuki = req.headers.cookie
  if(kuki && kuki.indexOf(tknLbl) != -1) return res.end(); // jika sudah ada token
  const bgn = Date.now() + 1000*count1
  const end = bgn + 1000*(count2 + latency)
  const payload = bgn.toString(36) + '.' + end.toString(36)
  const hmac = crypto.createHmac(algo, secret)
  hmac.update(payload)
  const sign = b64safe(hmac.digest('base64'))
  const token = sign + payload
  res.setHeader('Set-Cookie', `${tknLbl}${token}; expires=${(new Date(end)).toGMTString()}; HttpOnly; SameSite=Lax`)
  res.setHeader('Content-Type', 'text/html; charset=utf-8')
  res.end(`
    <span id="lblEl"></span>
    <h1 id="ctrEl"></h1>
    <span id="lnkEl"></span>
    <script>
      function down(ctr, lbl, lnk, idx){
        idx = idx || 0
        if(idx < ctr.length){
          lblEl.innerHTML = lbl[idx]
          var iv = setInterval(function(){
            if(ctr[idx]){
              ctrEl.innerHTML = ctr[idx]
              ctr[idx]--
            }else{
              lnkEl.innerHTML = lnk[idx]
              clearInterval(iv)
              down(ctr, lbl, lnk, idx + 1)
            }
          }, 1000)
        }
      }
      const ctr = [${count1}, ${count2}]
      const lbl = ['please wait ${count1} sec. to get API link :', 'please click link before ${count2} sec.']
      const lnk = ['<a href="/api">GET API</a>', '']
      down(ctr, lbl, lnk)
    </script>
  `)
}

const API = `{
  "prop_1": "val_1"
}`
function api(req, res, url){
  const kuki = req.headers.cookie
  if(kuki == undefined) return res.end(); // jika tidak ada cookie
  const pos1 = kuki.indexOf(tknLbl) + tknLblLen
  if(pos1 < tknLblLen) return res.end(); // jika tidak ada token
  const pos2 = kuki.indexOf(';', pos1)
  const token = pos2 == -1? kuki.substr(pos1): kuki.substr(pos1, pos2)
  if(token.length < 64) return res.end(); //jika token tidak valid
  const sign = token.substr(0, 64)
  const payload = token.substr(64)
  const hmac = crypto.createHmac(algo, secret)
  hmac.update(payload)
  if(b64safe(hmac.digest('base64')) != sign) return res.end(); // jika sign tidak valid
  const pl = payload.split('.')
  const bgn = parseInt(pl[0], 36)
  const end = parseInt(pl[1], 36)
  const now = Date.now()
  if(now < bgn || now > end) return res.end(); // jika diluar cakupan waktu yang valid
  res.setHeader('Cache-Control', 'no-store')
  res.setHeader('Content-Type', 'application/json')
  res.end(API) // response API
}

function notfound(req, res, url){
  res.statusCode = 404
  res.end('not found')
}

const rute = {
  '/': root,
  '/token': token,
  '/api': api
}
const PORT = 8080
require('http').createServer((req, res)=>{
  const url = req.url.split('?');
  (rute[url[0]] || notfound)(req, res, url)
}).listen(PORT)